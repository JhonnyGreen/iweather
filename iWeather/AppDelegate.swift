//
//  AppDelegate.swift
//  iWeather
//
//  Created by Семен Танасиенко on 02.10.2019.
//  Copyright © 2019 Windy.com. All rights reserved.
//

import UIKit

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.rootViewController = FirstViewController() // Name of your ViewController
        window?.makeKeyAndVisible()
        return true
    }
}
