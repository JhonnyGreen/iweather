//
//  ViewController.swift
//  iWeather
//
//  Created by Семен Танасиенко on 02.10.2019.
//  Copyright © 2019 Windy.com. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // MARK: - Init with coder -
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    //––––––––––––––––––––––––––––––––––––––––
    // MARK: - Deinit -
    deinit { debugPrint("🔻Deinit \(type(of: self))") }
    //––––––––––––––––––––––––––––––––––––––––
    // MARK: - Init -
    init() {
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle.main)
        debugPrint("🔺Init \(type(of: self))")
    }
}

